const inputs = document.querySelectorAll('.input-group > input, .input-group > textarea');
inputs.forEach(function (item) {
   item.addEventListener('focus', function (e) {
       this.parentNode.classList.add('input-hovered');
      this.parentNode.classList.remove('border-onblur')
   });
    item.addEventListener('blur', function (e) {
        console.log(e);
        if(!e.target.value){
            this.parentNode.classList.remove('input-hovered');
        }
        this.parentNode.classList.add('border-onblur');
    })
});
document.getElementById('scroll-down').addEventListener('click', function (e) {
    e.preventDefault();
});


function scrollIt(destination, duration = 500, callback) {
    function easeOutQuad(t) {
            return t * (2 - t);
        }
    const start = window.pageYOffset;
    const startTime = 'now' in window.performance ? performance.now() : new Date().getTime();
    const destinationOffsetToScroll = document.getElementById('main-content').offsetTop;

    function scroll() {
        const now = 'now' in window.performance ? performance.now() : new Date().getTime();
        const time = Math.min(1, ((now - startTime) / duration));
        const timeFunction = easeOutQuad(time);
        window.scroll(0, Math.ceil((timeFunction * (destinationOffsetToScroll - start)) + start));

        if (window.pageYOffset === destinationOffsetToScroll) {
            return;
        }
        requestAnimationFrame(scroll);
    }
    scroll();
}
document.querySelector('#scroll-down').addEventListener('click', function () {
    scrollIt(800)
} );