<header class="header">
    <div class="container">
        <div class="row">
            <div class="header__top">
                <img class="logo" src="../images/logo-ivaldi-white.svg" alt="">
                <button class="btn-text">Contact us</button>
            </div>
        </div>
    </div>

    <div class="header__text">
        <div class="container">
            <h1>
                Send files - not parts
            </h1>
            <div class="credo">We develop cost effective in-port additive manufacturing solutions.</div>
        </div>

    </div>
    <div class="header__btn-scroll-container">
        <a href="#main-content" id="scroll-down">learn more</a>
    </div>

</header>