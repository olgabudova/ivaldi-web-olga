<footer>
    <img class="logo" src="../images/ivaldi_logo_black.svg" alt="">
    <div class="copyright">Copyright © 2017 Ivaldi Group AS</div>
</footer>

<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/641/web-animations-next.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/641/web-animations.min.js"></script>
<script src="./js/main.js"></script>
</body>
</html>