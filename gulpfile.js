'use strict';

const vendorJs = [
    './node_modules/jquery/dist/jquery.js',
    './node_modules/tether/dist/js/tether.js',
    './node_modules/bootstrap/dist/js/bootstrap.js'
];

const vendorStyles = [];

const fileinclude = require('gulp-file-include'),
    gulp = require('gulp'),
    del = require('del'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    clean = require('gulp-clean'),
    server = require('browser-sync'),
    postcss    = require('gulp-postcss'),
    sourcemaps = require('gulp-sourcemaps');

server.create();

gulp.task('serve', () => {
    server.init({
    files: ['./dist/**/*'],
    port: 4000,
    server: {
        baseDir: './dist'
    }
    })
});

//Deleting previous build

gulp.task('clean', function () {
    return gulp.src('./dist')
        .pipe(clean());
});

gulp.task('clean:js', function() {
    return del('./dist/js/**/**');
});

gulp.task('clean:html', function() {
    return del('./dist/html/**/**');
});

gulp.task('clean:styles', function() {
    return del('./dist/styles/**/**');
});

//Task for build

gulp.task('copy', ['clean'], function() {
    gulp.src('./src/images/**')
        .pipe(gulp.dest('./dist/images/'));

    gulp.src(['./src/html/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./dist/'));
    gulp
        .src(['./src/sass/*.scss', '!src/sass/_*.scss'])
        .pipe(sass({outputStyle: 'nested' }))
        .pipe( postcss([ require('autoprefixer') ]))
        .pipe(gulp.dest('./dist/styles/'));
    gulp
        .src(['src/sass/libs/**/*.scss', '!src/sass/libs/**/_*.scss'])
        .pipe(sass({outputStyle: 'nested' }))
        .pipe(gulp.dest('./dist/styles/'));
    gulp
        .src(['src/js/*.js'])
        .pipe(gulp.dest('./dist/js/'));

    gulp.src(vendorJs)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('./dist/vendor'));

    return gulp.src(vendorStyles)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./dist/vendor'));
});

//Tasks called onchange of files in src

gulp.task('update:html', ['clean:html'], function() {
    return gulp.src(['./src/html/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('javascript', function() {
    return gulp
        .src(['src/js/*.js'])
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('styles', ['styles-libs'], function () {
    return gulp
        .src(['src/sass/*.scss', '!src/sass/_*.scss'])
        .pipe(sass({outputStyle: 'nested' }))
        .pipe( postcss([ require('autoprefixer') ]))
        .pipe(gulp.dest('./dist/styles/'));
});

gulp.task('styles-libs', ['clean:styles'], function() {
    return gulp
        .src(['src/sass/libs/*.scss', '!src/sass/libs/_*.scss'])
        .pipe(sass({outputStyle: 'nested' }))
        .pipe(gulp.dest('./dist/styles/'));
});

gulp.task('watch', function() {
    gulp.watch(['./src/html/**/*'], ['update:html']);
    gulp.watch('./src/sass/**/*.scss', ['styles']);
    gulp.watch('./src/js/**/*.js', ['clean:js', 'javascript']);
});
gulp.task('build', [
    'copy'
]);

gulp.task('default', [
    'serve',
    'watch'
]);